# Deploy and work with rest-api locally

### Install rest-api app from GitLab:  
```git clone https://gitlab.com/HavshynMaksym/rest_api.git```

### Docker and Docker-compose
If you have not  **Docker** or **Docker-compose**, you can set up Docker - [how to install](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru).
**Docker-compose** - [how to install](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-ru).

### Change .env file
You need rename  **".env.example"** to **".env"**:  
```cp .env.example .env```

And change params in **".env"** file:  
```nano .env```

You can find next params in the file :
```
#prod test staging dev
RUN_MODE=prod

#Django
PORT=8000
ALLOWED_HOST=xxxx:хххх
SECRET_KEY=xxxx

# Postgresql
DB_NAME=rest_api
DB_HOST=postgresql
DB_PASSWORD=xxxx
DB_USER=rest_api_admin
DB_PORT=5432

#Gunicorn
WORKERS=4

# Celery
CELERY_NUM_WORKERS=2
```
in "хххх" you need to put your own SECRET_KEY, DB_PASSWORD и ALLOWED_HOSTS, where:  
SECRET_KEY - secret key of project;  
DB_PASSWORD - secret key of database;  
ALLOWED_HOSTS - Your Heroku address;

Also you can change:  
PORT - nginx port;  
DB_PORT - database port;   
DB_NAME - name of database;  
DB_USER - admin name of database;  
DB_PORT - database port;  
WORKERS - instance workers for gunicorn.  
CELERY_NUM_WORKERS - instance workers for celery.  

P.S.  
For generation SECRET_KEY, you can use [this site](https://djecrety.ir/).  
Save in **nano**, use keyboard shortcut:  
```ctrl + o```  
Exit from **nano**, use keyboard shortcut:  
```ctrl + x```

## Work with project locally
### Build and start project with docker:  
```docker-compose up --build -d```


### Create database in postgresql
Go into container in **postgresql**:  
```docker-compose exec postgresql bash```

Go to the console **postgresql**:
```
su - postgres
psql
```
Create databse in **postgresql**, where your data must match to **".env"** file:
```
CREATE DATABASE rest_api; 
CREATE USER rest_api_admin WITH PASSWORD 'xxxx';
ALTER ROLE rest_api_admin SET client_encoding TO 'utf8';
ALTER ROLE rest_api_admin SET default_transaction_isolation TO 'read committed';
ALTER ROLE rest_api_admin SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE rest_api TO rest_api_admin;
ALTER USER rest_api_admin CREATEDB;
```
And at the end you must create connections in the database:  
```sudo docker-compose exec backend python manage.py migrate ```

Now you can use site locally at localhost address.

### Navigation
On the home page you see UI registration page, or you can register with API.
```
{host}/api/v1/auth/users/
```
+ You can create token:
```
{host}/api/v1/auth_token/token/login
```
+ You can see all post:  
```
{host}/api/v1/post/list/
```
+ You can create posts:  
```
{host}/api/v1/post/create/
```
+ You can modify or delete posts:
```
{host}/api/v1/post/update/{number_of_post}/
```
+ You can like posts:

```
{host}/api/v1/post/upvote/
```

Moreover, you can:  

+ Create comments:  
```
{host}/api/v1/comment/create/
```
+ Modify or delete yours comments:
```
{host}/api/v1/comment/update/{number_of_post}/
```

## DEPLOY 
### Sign up for Heroku 

You need registration on [Heroky](https://www.heroku.com/) and install [Heroky CLI](https://devcenter.heroku.com/articles/getting-started-with-python#set-up) app.

Next you need:
+ Login to Heroku. 
```
heroku login 
```  
+ Create a Heroku App
```
heroku create project_name
```
+ Login to the Heroku Container.
```
heroku container:login
```
+  Push and Release your app.
```
heroku container:push web
```
If all went well with the push, you can release it:
```
heroku container:release web
```
+ With heroku you can create migrations and super user:  
Migrate:
```
heroku run python3 manage.py migrate
```
Create super user:
```
heroku run python3 manage.py createsuperuser
```

### Depoloyed app:  https://rest-api-site.herokuapp.com/
