#!/bin/bash

rm /tmp/celerybeat-schedule /tmp/celerybeat.pid
celery -A rest_api beat -l info --schedule=/tmp/celerybeat-schedule --pidfile=/tmp/celerybeat.pid