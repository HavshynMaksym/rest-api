#!/bin/bash

gunicorn -w ${WORKERS} -b 0:${PORT} rest_api.wsgi:application
