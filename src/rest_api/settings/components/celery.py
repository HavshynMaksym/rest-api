from celery.schedules import crontab


CELERY_BROKER_URL = 'redis://redis:6379'
CELERY_RESULT_BACKEND = 'redis://redis:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'

CELERY_BEAT_SCHEDULE = {

    'task': {
        'task': 'rest_api.tasks.clean_upvote',
        'schedule': crontab(hour='*/24',),

    }

}
