import os

from rest_api.settings.components.base import * # noqa
from rest_api.settings.components.database import * # noqa

from rest_api.settings.components.celery import * # noqa


DEBUG = False

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '').split(':')

STATIC_ROOT = '/var/www/rest_api/static'

MEDIA_ROOT = '/var/www/rest_api/media'

SESSION_COOKIE_AGE = 2 * 3600

SECRET_KEY = os.environ.get('SECRET_KEY', 'SECRET_KEY')
