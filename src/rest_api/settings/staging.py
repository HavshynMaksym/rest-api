from rest_api.settings.components.base import * # noqa
from rest_api.settings.components.database import * # noqa

from rest_api.settings.components.celery import * # noqa


DEBUG = False

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

STATIC_ROOT = '/var/www/tmb/static'

MEDIA_ROOT = '/var/www/tmb/media'
