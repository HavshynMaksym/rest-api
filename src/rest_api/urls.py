"""rest_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

API_PREFIX = 'api/v1'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('accounts.urls')),

    path(f'{API_PREFIX}/', include('posts.api.urls')),
    path(f'{API_PREFIX}/api-auth/', include('rest_framework.urls')),
    path(f'{API_PREFIX}/auth/', include('djoser.urls')),
    path(f'{API_PREFIX}/auth_token/', include('djoser.urls.authtoken')),
]

handler404 = 'rest_api.views.error_404'
handler500 = 'rest_api.views.error_500'
handler403 = 'rest_api.views.error_403'
handler400 = 'rest_api.views.error_400'
