from django.shortcuts import render


def error_400(request, exception):
    data = {}
    return render(request, 'errs/400.html', data)


def error_404(request, exception):
    data = {}
    return render(request, 'errs/404.html', data)


def error_403(request, exception):
    data = {}
    return render(request, 'errs/403.html', data)


def error_500(request):
    data = {}
    return render(request, 'errs/500.html', data)
