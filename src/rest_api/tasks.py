from celery import shared_task

from posts.models import Post


@shared_task
def clean_upvote():
    upvote = Post.objects.update(upvotes=0)
    return upvote
