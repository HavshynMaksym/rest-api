from django.db.models import Sum

from posts.api.permissions import CommentOwnerOrReadOnly, PostOwnerOrReadOnly
from posts.api.serializers import CommentSerializer, PostActionSerializer, PostListSerializer, PostUpdateSerializer
from posts.models import Comment, Post, PostAction

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated


class PostListView(generics.ListAPIView):
    queryset = Post.objects.all().annotate(
        total_upvotes=Sum('postaction__upvotes')
    )
    serializer_class = PostListSerializer
    permission_classes = (IsAuthenticated,)


class PostListCreateView(generics.CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostUpdateSerializer


class PostListUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostUpdateSerializer
    permission_classes = (PostOwnerOrReadOnly,)


class CommentCreateView(generics.ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class CommentListUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (CommentOwnerOrReadOnly,)


class PostActionCreateView(generics.ListCreateAPIView):
    queryset = PostAction.objects.all()
    serializer_class = PostActionSerializer
