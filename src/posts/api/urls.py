from django.urls import path

from posts.api.views import (
    CommentCreateView,
    CommentListUpdateDeleteView,
    PostActionCreateView,
    PostListCreateView,
    PostListUpdateDeleteView,
    PostListView,
)

app_name = 'api_post'

urlpatterns = [
    path('post/list/', PostListView.as_view(), name='posts-list'),
    path('post/create/', PostListCreateView.as_view(), name='posts-create'),
    path('post/update/<int:pk>/', PostListUpdateDeleteView.as_view(), name='post_update'),
    path('comment/create/', CommentCreateView.as_view(), name='posts-list'),
    path('comment/update/<int:pk>/', CommentListUpdateDeleteView.as_view(), name='test-detail'),
    path('post/upvote/', PostActionCreateView.as_view(), name='upvote'),
]
