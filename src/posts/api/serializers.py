from posts.models import Comment, Post, PostAction

from rest_framework import serializers


class PostListSerializer(serializers.ModelSerializer):
    comments = serializers.SlugRelatedField(read_only=True, many=True, slug_field='content')
    author = serializers.SlugRelatedField(read_only=True, many=True, slug_field='username')
    total_upvotes = serializers.IntegerField()

    class Meta:
        model = Post
        fields = (
            'title',
            'link',
            'create_date',
            'author',
            'comments',
            'total_upvotes'
        )


class PostUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'title',
            'link',
            'create_date',
            'author',
        )


class CommentSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Comment
        fields = (
            'content',
            'create_date',
            'user',
            'post',
        )


class PostActionSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = PostAction
        fields = (
            'user',
            'upvotes',
            'post',
        )
