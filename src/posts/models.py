from accounts.models import User

from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=64)
    link = models.URLField(max_length=200)
    create_date = models.DateTimeField(null=True, auto_now_add=True)
    author = models.ManyToManyField(to=User, related_name='posts')

    def __str__(self):
        return f'{self.title}'


class Comment(models.Model):
    content = models.TextField(max_length=200)
    create_date = models.DateTimeField(null=True, auto_now_add=True)
    user = models.ForeignKey(to=User, related_name='comments', on_delete=models.CASCADE)
    post = models.ForeignKey(to=Post, related_name='comments', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}'


class PostAction(models.Model):
    class POST_ACTION_TYPES(models.IntegerChoices):
        DISLIKE = -1, "Dislike"
        LIKE = 1, "Like"

    upvotes = models.IntegerField(choices=POST_ACTION_TYPES.choices, default=0)
    user = models.ForeignKey(to=User, related_name='postaction', on_delete=models.CASCADE)
    post = models.ForeignKey(to=Post, related_name='postaction', on_delete=models.CASCADE)
