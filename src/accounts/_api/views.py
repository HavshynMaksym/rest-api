from accounts.api.serializers import UserSerializer

from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['POST', ])
def registration(request):
    if request.method == 'POST':
        serializer = UserSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            user = serializer.save()
            data['response'] = "successfully registered a new user."
            data['username'] = user.username
        else:
            data = serializer.errors
        return Response(data)
