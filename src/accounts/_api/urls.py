from accounts.api.views import registration

from django.urls import path

app_name = 'account'

urlpatterns = [
    path('registration/', registration, name='registration'),


]
