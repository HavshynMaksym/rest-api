from accounts.forms import AccountCreateForm
from accounts.models import User

from django.views.generic import CreateView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = "/api/v1/api-auth/login/"
