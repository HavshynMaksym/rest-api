from accounts.models import User

from django.contrib.auth.forms import UserCreationForm


class AccountCreateForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ['username', 'password1', 'password2']
