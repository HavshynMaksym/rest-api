from accounts.views import AccountCreateView

from django.urls import path

app_name = 'accounts'

urlpatterns = [
    path('', AccountCreateView.as_view(), name='register'),

]
